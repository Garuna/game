#include "main.h"

const color_t COLOR_RED = { 255, 40, 15 };
const color_t COLOR_GREEN = { 63, 225, 64 };
const color_t COLOR_BLACK = { 52, 73, 94 };
const color_t COLOR_BACKGROUND = { 242, 241, 239 };
const color_t COLOR_BROWN = {89,69,19};
const color_t COLOR_YELLOW = {255 , 242 , 0};
const color_t COLOR_BLUE = {0,191,255};
const color_t COLOR_DEEP_RED = {210,9,9};
const color_t COLOR_PINK = {255,20,147};
