#include "main.h"

#ifndef Trampo_H
#define Trampo_H


class Trampo {
public:
    Trampo() {}
    Trampo(float x, float y ,color_t color);
    glm::vec3 position;
    float rotation;
    float radius ;
    bool existance ;

    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    void tick();
    double speed;

    bounding_box_t bounding_box();
private:
    VAO *object;
};

#endif // Trampo_H
