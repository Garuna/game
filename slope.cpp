#include "slope.h"
#include "math.h"
#include "main.h"


Slope::Slope(float x, float y , float t, color_t color) {
    this->position = glm::vec3(x, y, 0);
    this->position.x = x;
    this->position.y = y;
    this->existance = 1;

    this->rotation = t;

//    this->angle = t;
    this->speed = 0;
    this->speedy = 0;

    GLint i ,j = 0;
    static const GLfloat g_vertex_buffer_data[] {

         0, 0, 0, // vertex 1
         0, -.4, 0, // vertex 2
         4.2,  0, 0, // vertex 3

         4.2,  0, 0, // vertex 3
         0,  -.4, 0, // vertex 4
        4.2, -.4, 0 // vertex 1


   };

    this->object = create3DObject(GL_TRIANGLES, 3 * 2, g_vertex_buffer_data, color, GL_FILL);

}

void Slope::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(0, 0, 1));
    rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
}

void Slope::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

void Slope::tick() {
    float k =  rand()%2 - 3;
    this->position.x = rand()%10 - 5 ;
    this->position.y =  1 + rand()%3;
    if (k)
        this->rotation = (rand()%60) ;
    else
        this->rotation = -rand()%60;

}



bounding_box_t Slope::bounding_box() {
    float x = this->position.x, y = this->position.y;
    bounding_box_t bbox = { x, y, 4.2 , 0.4 , this->rotation};
    return bbox;
}
