#include "main.h"
#include "timer.h"
#include "ball.h"
#include "slope.h"
#include "ground.h"
#include "trampo.h"
#include "spike.h"
#include "water.h"
#include "magnet.h"
#include "powerup.h"


using namespace std;

GLMatrices Matrices;
GLuint     programID;
GLFWwindow *window;

/**************************
* Customizable functions *
**************************/

Ball ball1;
Ball ball[25];
GLint i;
Slope slope;
Ground ground;
Trampo trampo;
Water water;
Spike spike[2];
Magnet magnet;
Powerup power;
int score = 0 , level = 0;
float screen_zoom = 1, screen_center_x = 0, screen_center_y = 0;
int score_prev =0 ;
Timer t60(1.0 / 60);

/* Render the scene with openGL */
/* Edit this function according to your assignment */
void draw() {
    // clear the color and depth in the frame buffer
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // use the loaded shader program
    // Don't change unless you know what you are doing
    glUseProgram (programID);

    // Eye - Location of camera. Don't change unless you are sure!!
    // glm::vec3 eye ( 5*cos(camera_rotation_angle*M_PI/180.0f), 0, 5*sin(camera_rotation_angle*M_PI/180.0f) );
    // Target - Where is the camera looking at.  Don't change unless you are sure!!
    // glm::vec3 target (0, 0, 0);
    // Up - Up vector defines tilt of camera.  Don't change unless you are sure!!
    // glm::vec3 up (0, 1, 0);

    // Compute Camera matrix (view)
    // Matrices.view = glm::lookAt( eye, target, up ); // Rotating Camera for 3D
    // Don't change unless you are sure!!
    Matrices.view = glm::lookAt(glm::vec3(0, 0, 3), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0)); // Fixed camera for 2D (ortho) in XY plane

    // Compute ViewProject matrix as view/camera might not be changed for this frame (basic scenario)
    // Don't change unless you are sure!!
//    glm::mat4 VP = Matrices.projection * Matrices.view;
    glm::mat4 VP = Matrices.projection * Matrices.view * glm::scale(glm::vec3(exp(zoom)));

    // Send our transformation to the currently bound shader, in the "MVP" uniform
    // For each model you render, since the MVP will be different (at least the M part)
    // Don't change unless you are sure!!
    glm::mat4 MVP;  // MVP = Projection * View * Model


    // Scene render
//    cout<<"aa"<<endl;

    ground.draw(VP);
    water.draw(VP);
    if (power.existance)
         power.draw(VP);
    ball1.draw(VP);
    spike[0].draw(VP);
    spike[1].draw(VP);
    magnet.draw(VP);
    for(i = 0 ; i < 25 ;i++){
        if (ball[i].existance)
        ball[i].draw(VP);
    }
    if (slope.existance)
        slope.draw(VP);
    trampo.draw(VP);

}


void tick_input(GLFWwindow *window) {

    int left  = glfwGetKey(window, GLFW_KEY_A);
    int right = glfwGetKey(window, GLFW_KEY_D);
    int up = glfwGetKey(window, GLFW_KEY_SPACE);
    int mouse_clicked = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);

    int key_a = glfwGetKey(window, GLFW_KEY_LEFT);
    int key_s = glfwGetKey(window, GLFW_KEY_DOWN);
    int key_d = glfwGetKey(window, GLFW_KEY_RIGHT);
    int key_w = glfwGetKey(window, GLFW_KEY_UP);


    if (mouse_clicked) {
        if (drag_oldx == -1 && drag_oldy == -1) {
            glfwGetCursorPos(window, &drag_oldx, &drag_oldy);
        }
        else {
            int w, h;
            double new_x, new_y;
            glfwGetCursorPos(window, &new_x, &new_y);
            glfwGetWindowSize(window, &w, &h);
            float pos_x, pos_y;
            pos_x = .7 * (new_x - drag_oldx) / (w * screen_zoom);

            ball1.position.x += pos_x;
            if(ball1.position.x >= 7.3)
                ball1.position.x = 7.3;
            if(ball1.position.x <= -7.3)
                ball1.position.x = -7.3;

        }
    }

    if (left) {
        if(ball1.position.x - .75 >= -8)
        {
        ball1.set_position(ball1.position.x - .05 , ball1.position.y );
        }
    }
    if (right) {
        if(ball1.position.x + .75 <= 8)
        {
        ball1.set_position(ball1.position.x + .05 , ball1.position.y );
        }
    }
      if (up && !ball1.isup) {
          if(ball1.inwater)
              ball1.speedy += .2;
          else
            {
             ball1.speedy = .25;
             ball1.isup =  1;
            }

      }

      if (key_a && screen_center_x - 9/screen_zoom > -9) {
          screen_center_x -= 0.07;
          reset_screen();
      }
      if (key_s && screen_center_y - 8/screen_zoom > -8) {
          screen_center_y -= 0.07;
          reset_screen();
      }
      if (key_d && screen_center_x + 9/screen_zoom < 9) {
          screen_center_x += 0.07;
          reset_screen();
      }
      if (key_w && screen_center_y + 10/screen_zoom < 10) {
          screen_center_y += 0.07;
          reset_screen();
      }

}

void tick_elements() {

    if(score > score_prev )
        cout<<score<<" "<<ball1.lives<<endl;
    if (ball1.lives < 0)
        exit(0);

    if(score > 200)
        magnet.tick();
    if(magnet.position.y <= 5 && magnet.speedy < 0)
        magnet.speedy = 0.5;

    if(magnet.position.y >= 100 && magnet.speedy > 0)
        magnet.speedy = -0.03;


    if(spike[0].position.x > -5.8)
        if (score > 200 )
             spike[0].speed = .15;
        else
            spike[0].speed = .05;

    if((score/100) > score_prev/100 )
    {
        power.existance =1;
        score_prev = score;
    }
    if(spike[1].position.x < 1.8)
        if (score > 200 )
             spike[1].speed = -.15;
        else
            spike[1].speed = -.05;

    spike[0].tick();
    spike[1].tick();
    if (ball1.position.x < -7.3)
        ball1.position.x = -7.3;
    if (ball1.position.x > 7.3)
        ball1.position.x = 7.3;

    if(abs(ball1.position.x - water.position.x) > 2.4 || ball1.position.y >= 0)
    {
        ball1.inwater = 0;

    }
    if(!ball1.isup && !ball1.inwater)
        ball1.position.y = 0 ;

    ball1.tick();

    slope.existance =1;
    if (score < 100)
        slope.existance = 0;


    if(!ball1.isup){
        ball1.speed = 0;
        ball1.speedy = 0;
        }

    if(detect_collision_magnet( ball1.bounding_box(), magnet.bounding_box()) )
    {
        ball1.position.x += 0.06;
    }

    if (detect_collision_ground(ball1.bounding_box() , ground.bounding_box()) && !ball1.inwater && ball1.speedy < 0){

        ball1.isup = 0;
        ball1.speedy = 0;
        ball1.position.y = 0 ;
        ball1.speed = 0 ;
    }

    if (detect_collision_water(ball1.bounding_box() , water.bounding_box()) ){

        if(ball1.position.y + sqrt(-(ball1.position.x - water.position.x)*(ball1.position.x - water.position.x) + 2.4*2.4) > .2)
            ball1.speedy -= .05;
        else
        {
            ball1.speedy = 0;
            if(!ball1.isup)
                ball1.position.y =  -sqrt(-(ball1.position.x - water.position.x)*(ball1.position.x - water.position.x) + 2.4*2.4);
        }
        ball1.inwater = 1;
    }

    if (detect_collision_trampo(ball1.bounding_box() , trampo.bounding_box()) && ball1.speedy < 0){
        ball1.speedy = -ball1.speedy;
        ball1.speedy += .1 ;
        ball1.speed = 0;

    }
    power.tick();

    if (detect_collision_trampo(ball1.bounding_box() , power.bounding_box()) && power.existance){
        power.existance = 0;
        ball1.lives ++ ;

    }


    if(detect_collision_slope(ball1.bounding_box() , slope.bounding_box() , slope.rotation) && slope.existance && ball1.speedy < 0)
    {
        ball1.speedy = -ball1.speedy + .1;
        ball1.speed = ball1.speedy*sin(2*slope.rotation*3.14159/180);
        ball1.speedy = ball1.speedy*cos(2*slope.rotation*3.14159/180) ;
        slope.existance = 0;
        score += 10;
        slope.tick();

    }

    for(i = 0 ; i<2 ;i ++){
    if(detect_collision_spike(ball1.bounding_box() , spike[i].bounding_box()) && ball1.speedy < 0)
    {
        ball1.lives --;
        ball1.set_position(-2 ,-0.7);

    }
}


    if(spike[0].position.x < -8.5 && spike[0].speed > 0){
        spike[0].position.x = -rand()%10 - 12;
        spike[0].speed = -0.05;
    }

    if(spike[1].position.x > 8.5 && spike[1].speed < 0){
        spike[1].position.x = rand()%10 + 12;
        spike[1].speed = 0.05;
    }

    for(i = 0 ; i < 25 ;i++){
    ball[i].tick();
    ball[i].speedy = 0;

    if(ball[i].position.x > 8.2)
      {
        ball[i] = Ball(-rand()%18 - 8 , 2 + rand()%7, .2*(rand()%4) , COLOR_RED);
        ball[i].speed -= .02 + .03*(rand()%2);
        ball[i].existance = 1;
        }
    }

    for(i = 0 ; i < 25 ;i++){
    if (detect_collision(ball1.bounding_box(), ball[i].bounding_box()) && ball[i].existance && ball1.speedy < 0) {
        ball[i].existance = 0;
        ball1.speedy = .25;
        ball1.speed = 0;
        // Update score here
        score+=5;


    }
   }

}

/* Initialize the OpenGL rendering properties */
/* Add all the models to be created here */
void initGL(GLFWwindow *window, int width, int height) {
    /* Objects should be created before any other gl function and shaders */
    // Create the models
    srand (time(NULL));
    ball1 = Ball(2, 0, 0.7 , COLOR_GREEN);
    for(i = 0 ; i < 25 ;i++){

        ball[i] = Ball(-rand()%18 - 3 , 1 + rand()%7, .3*(rand()%4) , COLOR_RED);
        ball[i].speed -= .2 + .3*(rand()%2);

    }
    slope = Slope(rand()%10 - 5 , 1 + rand()%3 , rand()%60 , COLOR_BROWN);
    spike[0] = Spike(-rand()%10 - 8 , -0.7 ,COLOR_BLACK);
    spike[1] = Spike(rand()%10 + 8 , -0.7 ,COLOR_BLACK);
    spike[0].speed = -.05;
    spike[1].speed = .05;
    magnet = Magnet(8 , 90, 1.5 , COLOR_DEEP_RED);
    magnet.speedy  = -0.03;
    ground = Ground( 0 , -0.7 , COLOR_BLACK);
    water = Water(-2 , -0.7 , COLOR_BLUE);
    trampo = Trampo(4 , 0.5 , COLOR_YELLOW );
    power = Powerup( 4 - rand()%8 , 3 + rand()%1 , COLOR_PINK);
    // Create and compile our GLSL program from the shaders
    programID = LoadShaders("Sample_GL.vert", "Sample_GL.frag");
    // Get a handle for our "MVP" uniform
    Matrices.MatrixID = glGetUniformLocation(programID, "MVP");


    reshapeWindow (window, width, height);

    // Background color of the scene
    glClearColor (COLOR_BACKGROUND.r / 256.0, COLOR_BACKGROUND.g / 256.0, COLOR_BACKGROUND.b / 256.0, 0.0f); // R, G, B, A
    glClearDepth (1.0f);

    glEnable (GL_DEPTH_TEST);
    glDepthFunc (GL_LEQUAL);

    cout << "VENDOR: " << glGetString(GL_VENDOR) << endl;
    cout << "RENDERER: " << glGetString(GL_RENDERER) << endl;
    cout << "VERSION: " << glGetString(GL_VERSION) << endl;
    cout << "GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
}


int main(int argc, char **argv) {
    srand(time(0));
    int width  = 600;
    int height = 600;

    window = initGLFW(width, height);

    initGL (window, width, height);

    /* Draw in loop */
    while (!glfwWindowShouldClose(window)) {
        // Process timers

        if (t60.processTick()) {
            // 60 fps
            // OpenGL Draw commands
            draw();
            // Swap Frame Buffer in double buffering
            glfwSwapBuffers(window);

            tick_elements();
            tick_input(window);
        }

        // Poll for Keyboard and mouse events
        glfwPollEvents();
    }

    quit(window);
}

bool detect_collision(bounding_box_t a, bounding_box_t b) {
    return (abs(a.x - b.x) * 2.6 <= a.width + b.width) &&
           ((a.y - b.y)  <= a.height + b.height) && (a.y - b.y) >= 0;
}


bool detect_collision_ground(bounding_box_t a, bounding_box_t b) {
    return (abs(a.x - b.x) * 2 < (a.width + b.width)) &&
           (abs(a.y - b.y) * 2 < (a.height + b.height));
}

bool detect_collision_slope(bounding_box_t a, bounding_box_t b , float t) {
    return ((a.x - b.x)   <= b.width*cos(t*3.14159/180)) && a.x - b.x > 0 &&
           ((a.y - (b.y + (a.x - b.x)*tan(t*3.14159/180)))  <= (a.height - b.height/2)/cos(t*3.14159/180) ) && a.y > 0;
}

bool detect_collision_trampo(bounding_box_t a, bounding_box_t b){
    return (abs(a.x - b.x) <= 1 &&
           ((a.y - b.y))  <= ( a.height ));

}

bool detect_collision_water(bounding_box_t a, bounding_box_t b){
    return (abs(a.x - b.x) < 2.4 &&
           ((a.y))  <= 0);

}

bool detect_collision_spike(bounding_box_t a, bounding_box_t b){
    return (abs(a.x - b.x) < b.width  &&
           ((a.y + b.y))  <= 0.01);

}

bool detect_collision_magnet(bounding_box_t a, bounding_box_t b){
    return ((abs(a.y - b.y))  <= b.height);

}

bool detect_collision_power(bounding_box_t a, bounding_box_t b){
    return (((abs(a.x - b.x))*2  <= b.width) && (abs(a.y -b.y) * 2<=b.height))  ;

}




void reset_screen() {
    float top    = screen_center_y + 10  / screen_zoom;
    float bottom = screen_center_y - 8 / screen_zoom;
    float left   = screen_center_x - 9 / screen_zoom;
    float right  = screen_center_x + 9 / screen_zoom;
    Matrices.projection = glm::ortho(left, right, bottom, top, 0.1f, 500.0f);
}
