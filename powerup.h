#include "main.h"
#include<stdio.h>

#ifndef Powerup_H
#define Powerup_H


class Powerup {
public:
    Powerup() {}
    Powerup(float x, float y,color_t color);
    glm::vec3 position;
    float rotation;
    float radius ;
    float speedy;
    bool existance ;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    void tick();
    double speed;
    bounding_box_t bounding_box();
private:
    VAO *object;
};

#endif // Powerup_H
