#include "main.h"
#include<stdio.h>

#ifndef BALL_H
#define BALL_H


class Ball {
public:
    Ball() {}
    Ball(float x, float y, float r ,color_t color);
    glm::vec3 position;
    float rotation;
    float radius ;
    bool isup ;
    float speedy;
    bool existance ;
    float inwater;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    void tick();
    double speed;
    double lives;
    bounding_box_t bounding_box();
private:
    VAO *object;
};

#endif // BALL_H
