#include "spike.h"
#include "math.h"
#include "main.h"


Spike::Spike(float x, float y , color_t color) {
//    printf("qq\n");
    this->position = glm::vec3(x, y, 0);
    this->position.x = x;
    this->position.y = y;
    this->existance = 0;
    this->radius = 0;
    this->rotation = 0;
    this->speed = 0;
    this->speedy = 0 ;
    GLint i ,j = 0;
    static const GLfloat g_vertex_buffer_data[]{
        0 , 0 , 0,
        1 , 0 , 0,
        0.5 , .7 , 0,

        0 , 0 , 0,
        -1 , 0 , 0,
        -0.5 , .7 , 0,


    };


    this->object = create3DObject(GL_TRIANGLES, 3 * 2, g_vertex_buffer_data, color, GL_FILL);

}

void Spike::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(0, 0, 1));
    rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
}

void Spike::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

void Spike::tick() {
    this->position.x -= this->speed;
    this->position.y += this->speedy;

}

bounding_box_t Spike::bounding_box() {
    float x = this->position.x, y = this->position.y;
    bounding_box_t bbox = { x, y, 2 , .7 };
    return bbox;
}
