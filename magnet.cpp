#include "magnet.h"
#include "math.h"
#include "main.h"


Magnet::Magnet(float x, float y , float r, color_t color) {

    this->position = glm::vec3(x, y, 0);
    this->position.x = x;
    this->position.y = y;
    this->existance = 0;
    this->radius = r;
    this->rotation = 0;
    this->lives = 3;
    this->speed = 0;
    this->speedy = 0 ;
    GLint i = -26 ,j = 0;
    GLfloat g_vertex_buffer_data[20000];


    g_vertex_buffer_data[j++] = this->radius*(float)cos((double)M_PI*(i)/50);
    g_vertex_buffer_data[j++] = this->radius*(float)sin((double)M_PI*(i)/50);
    g_vertex_buffer_data[j++] = 0.0f;

    g_vertex_buffer_data[j++] = this->radius*(float)cos((double)M_PI*(i)/50) - this->radius/2;
    g_vertex_buffer_data[j++] = this->radius*(float)sin((double)M_PI*(i)/50);
    g_vertex_buffer_data[j++] = 0.0f;

    g_vertex_buffer_data[j++] = this->radius*(float)cos((double)M_PI*(i)/50) - this->radius/2;
    g_vertex_buffer_data[j++] = this->radius*(float)sin((double)M_PI*(i)/50) + this->radius /2;
    g_vertex_buffer_data[j++] = 0.0f;

    g_vertex_buffer_data[j++] = this->radius*(float)cos((double)M_PI*(i)/50) - this->radius/2;
    g_vertex_buffer_data[j++] = this->radius*(float)sin((double)M_PI*(i)/50) + this->radius/2;
    g_vertex_buffer_data[j++] = 0.0f;

    g_vertex_buffer_data[j++] = this->radius*(float)cos((double)M_PI*(i)/50);
    g_vertex_buffer_data[j++] = this->radius*(float)sin((double)M_PI*(i)/50) + this->radius/2;
    g_vertex_buffer_data[j++] = 0.0f;

    g_vertex_buffer_data[j++] = this->radius*(float)cos((double)M_PI*(i)/50) ;
    g_vertex_buffer_data[j++] = this->radius*(float)sin((double)M_PI*(i)/50) ;
    g_vertex_buffer_data[j++] = 0.0f;



    for ( i = -25 ; i <  25; i++){
        g_vertex_buffer_data[j++] = this->radius*(float)cos((double)M_PI*(i)/50)/2;
        g_vertex_buffer_data[j++] = this->radius*(float)sin((double)M_PI*(i)/50)/2;
        g_vertex_buffer_data[j++] = 0.0f;

        g_vertex_buffer_data[j++] = this->radius*(float)cos((double)M_PI*(i+1)/50)/2;
        g_vertex_buffer_data[j++] = this->radius*(float)sin((double)M_PI*(i+1)/50)/2;
        g_vertex_buffer_data[j++] = 0.0f;


        g_vertex_buffer_data[j++] = this->radius*(float)cos((double)M_PI*(i)/50);
        g_vertex_buffer_data[j++] = this->radius*(float)sin((double)M_PI*(i)/50);
        g_vertex_buffer_data[j++] = 0.0f;


        g_vertex_buffer_data[j++] = this->radius*(float)cos((double)M_PI*(i+1)/50)/2;
        g_vertex_buffer_data[j++] = this->radius*(float)sin((double)M_PI*(i+1)/50)/2;
        g_vertex_buffer_data[j++] = 0.0f;


        g_vertex_buffer_data[j++] = this->radius*(float)cos((double)M_PI*(i)/50);
        g_vertex_buffer_data[j++] = this->radius*(float)sin((double)M_PI*(i)/50);
        g_vertex_buffer_data[j++] = 0.0f;


        g_vertex_buffer_data[j++] = this->radius*(float)cos((double)M_PI*(i+1)/50);
        g_vertex_buffer_data[j++] = this->radius*(float)sin((double)M_PI*(i+1)/50);
        g_vertex_buffer_data[j++] = 0.0f;

   }

    g_vertex_buffer_data[j++] = this->radius*(float)cos((double)M_PI*(i+1)/50);
    g_vertex_buffer_data[j++] = this->radius*(float)sin((double)M_PI*(i+1)/50);
    g_vertex_buffer_data[j++] = 0.0f;

    g_vertex_buffer_data[j++] = this->radius*(float)cos((double)M_PI*(i+1)/50) - this->radius/2;
    g_vertex_buffer_data[j++] = this->radius*(float)sin((double)M_PI*(i+1)/50);
    g_vertex_buffer_data[j++] = 0.0f;

    g_vertex_buffer_data[j++] = this->radius*(float)cos((double)M_PI*(i+1)/50) - this->radius/2;
    g_vertex_buffer_data[j++] = this->radius*(float)sin((double)M_PI*(i+1)/50) - this->radius /2;
    g_vertex_buffer_data[j++] = 0.0f;

    g_vertex_buffer_data[j++] = this->radius*(float)cos((double)M_PI*(i+1)/50) - this->radius/2;
    g_vertex_buffer_data[j++] = this->radius*(float)sin((double)M_PI*(i+1)/50) - this->radius/2;
    g_vertex_buffer_data[j++] = 0.0f;

    g_vertex_buffer_data[j++] = this->radius*(float)cos((double)M_PI*(i+1)/50);
    g_vertex_buffer_data[j++] = this->radius*(float)sin((double)M_PI*(i+1)/50) - this->radius/2;
    g_vertex_buffer_data[j++] = 0.0f;

    g_vertex_buffer_data[j++] = this->radius*(float)cos((double)M_PI*(i+1)/50) ;
    g_vertex_buffer_data[j++] = this->radius*(float)sin((double)M_PI*(i+1)/50) ;
    g_vertex_buffer_data[j++] = 0.0f;


    this->object = create3DObject(GL_TRIANGLES, 6 * 54, g_vertex_buffer_data, color, GL_FILL);

}

void Magnet::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(0, 0, 1));
    rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
}

void Magnet::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

void Magnet::tick() {
    this->position.x -= this->speed;
    this->position.y += this->speedy;

}

bounding_box_t Magnet::bounding_box() {
    float x = this->position.x, y = this->position.y;
    bounding_box_t bbox = { x, y, this->radius, this->radius };
    return bbox;
}
