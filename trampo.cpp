#include "trampo.h"
#include "math.h"
#include "main.h"


Trampo::Trampo(float x, float y , color_t color) {
    this->position = glm::vec3(x, y, 0);
    this->position.x = x;
    this->position.y = y;
    this->existance = 0;

    this->rotation = 0;

    this->speed = 0;
    GLint i ,j = 0;
    GLfloat g_vertex_buffer_data[2000];

    for ( i = 0 ; i <  50; i++){
        g_vertex_buffer_data[j++] = 0.0;
        g_vertex_buffer_data[j++] = 0.0f;
        g_vertex_buffer_data[j++] = 0.0f;


        g_vertex_buffer_data[j++] = -.7*(float)cos((double)M_PI*(i)/50);
        g_vertex_buffer_data[j++] = -.7*(float)sin((double)M_PI*(i)/50);
        g_vertex_buffer_data[j++] = 0.0f;

        g_vertex_buffer_data[j++] = -.7*(float)cos((double)M_PI*(i+1)/50);
        g_vertex_buffer_data[j++] = -.7*(float)sin((double)M_PI*(i+1)/50);
        g_vertex_buffer_data[j++] = 0.0f;


   }

    g_vertex_buffer_data[j++] = -0.7 ;
    g_vertex_buffer_data[j++] =  0 ;
    g_vertex_buffer_data[j++] = 0 ;

    g_vertex_buffer_data[j++] = -0.7 ;
    g_vertex_buffer_data[j++] = -1 ;
    g_vertex_buffer_data[j++] = 0 ;

    g_vertex_buffer_data[j++] = -1 ;
    g_vertex_buffer_data[j++] =  0 ;
    g_vertex_buffer_data[j++] = 0 ;

    g_vertex_buffer_data[j++] = -.7 ;
    g_vertex_buffer_data[j++] = -1 ;
    g_vertex_buffer_data[j++] = 0 ;

    g_vertex_buffer_data[j++] =  -1 ;
    g_vertex_buffer_data[j++] =  0 ;
    g_vertex_buffer_data[j++] = 0 ;

    g_vertex_buffer_data[j++] = -1 ;
    g_vertex_buffer_data[j++] =  -1 ;
    g_vertex_buffer_data[j++] = 0 ;

\
    g_vertex_buffer_data[j++] = 0.7 ;
    g_vertex_buffer_data[j++] =  0 ;
    g_vertex_buffer_data[j++] = 0 ;

    g_vertex_buffer_data[j++] = 0.7 ;
    g_vertex_buffer_data[j++] = -1 ;
    g_vertex_buffer_data[j++] = 0 ;

    g_vertex_buffer_data[j++] = 1 ;
    g_vertex_buffer_data[j++] =  0 ;
    g_vertex_buffer_data[j++] = 0 ;

    g_vertex_buffer_data[j++] = .7 ;
    g_vertex_buffer_data[j++] = -1 ;
    g_vertex_buffer_data[j++] = 0 ;

    g_vertex_buffer_data[j++] =  1 ;
    g_vertex_buffer_data[j++] =  0 ;
    g_vertex_buffer_data[j++] = 0 ;

    g_vertex_buffer_data[j++] = 1 ;
    g_vertex_buffer_data[j++] = -1 ;
    g_vertex_buffer_data[j++] = 0 ;

    this->object = create3DObject(GL_TRIANGLES, 3*56, g_vertex_buffer_data, color, GL_FILL);

}

void Trampo::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(0, 0, 1));
    rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
}

void Trampo::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

void Trampo::tick() {
    this->position.x -= this->speed;

}

bounding_box_t Trampo::bounding_box() {
    float x = this->position.x, y = this->position.y;
    bounding_box_t bbox = { x, y, 4, 0.6 };
    return bbox;
}
